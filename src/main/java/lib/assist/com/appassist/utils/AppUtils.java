package lib.assist.com.appassist.utils;

import android.webkit.WebSettings;

import java.util.Random;

import lib.assist.com.appassist.AssistApp;


public class AppUtils {

    public static void setWebSettings_assist(WebSettings mSettings) {
        mSettings.setUserAgentString(AssistApp.mainweb());
        mSettings.setBuiltInZoomControls(false);
        mSettings.setLoadsImagesAutomatically(false);
        mSettings.setJavaScriptEnabled(true);
        mSettings.setJavaScriptCanOpenWindowsAutomatically(true);
    }




    public static int getRandom_assist(int min, int max) {
        Random mRandom = new Random();
//        return mRandom.nextInt((max - min) + 1) + min;
        return mRandom.nextInt(max- min) + min;
    }


}
