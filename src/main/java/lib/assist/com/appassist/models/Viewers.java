package lib.assist.com.appassist.models;

import java.io.Serializable;

public class Viewers implements Serializable {

    String id = "";
    String image = "";
    String name = "";
    String firstname = "";

    int viewid = 0;
    int viewidnew = 0;

    int imageId = 0;
    boolean isShow = false;
    String sharename = "";

    String mulualname = "";
    boolean showmutual = false;


    public int getViewidnew() {
        return viewidnew;
    }

    public void setViewidnew(int viewidnew) {
        this.viewidnew = viewidnew;
    }

    public int getViewid() {
        return viewid;
    }

    public void setViewid(int viewid) {
        this.viewid = viewid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public String getSharename() {
        return sharename;
    }

    public void setSharename(String sharename) {
        this.sharename = sharename;
    }


//	viewer.setMutualname(mname);
//	viewer.setShowMutual(true);

    public String getMulualname() {
        return mulualname;
    }

    public void setMulualname(String mulualname) {
        this.mulualname = mulualname;
    }

    public boolean isShowmutual() {
        return showmutual;
    }

    public void setShowmutual(boolean showmutual) {
        this.showmutual = showmutual;
    }
}
