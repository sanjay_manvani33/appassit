package lib.assist.com.appassist;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// git add appassit
//  git pull --recurse-submodules

//public class AssistApp extends MultiDexApplication {
public class AssistApp extends Application {

    /**
     * On application startup, override system default locale to which user set in application preference.
     */
     //  this is test message
    @Override
    public void onCreate() {
        super.onCreate();

//        Mint.initAndStartSession(getApplicationContext(), "9bf33e8e");
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }




    static {
        System.loadLibrary("assistglob");
    }

    /* -------------------------------------------------------------------------------
                                        SplashActivity
       -------------------------------------------------------------------------------
    */
    public static native String keyString1();

    public static native String getApp();

    public static native String getAppId();

    public static native String keyString2();

    /* -------------------------------------------------------------------------------
                                        LoginActivityOld
       -------------------------------------------------------------------------------
    */
    public static native String keyid();

    public static native String getimagel();

    public static native String getimagel1();

    /* -------------------------------------------------------------------------------
                                    MainActivity
       -------------------------------------------------------------------------------
    */
    public static native String getNativeKey1();

    public static native String getNativeKey2();

    public static native String getNativeKey3();

    public static native String getNativeKey4();

    public static native String getNativeKey5();

    public static native String getNativeKey6();

    public static native String getNativeKey7();

    public static native String getNativeKey8();

    public static native String getNativeKey9();

    public static native String getNativeKey10();

    public static native String getNativeKey11();

    public static native String getNativeKey12();

    public static native String getimage();

    public static native String getimage1();

    public static native String geturl();

    public static native String geturl1();

    public static native String getstringvalue();

    public static native String getstringvalue1();

    public static native String getstringvalue2();

    public static native String getstringvalue3();

    public static native String getstringvalue4();

    public static native String getstringvalue5();

    public static native String getstringvalue6();

    public static native String getstringvalue7();

    public static native String getsplit();

    public static native String getconstant();

    public static native String register();

    public static native String adduser();

    public static native String uniqueid();

    public static native String setgcm();

    public static native String mainweb();

    public static native String common();

    public static native String commonshare();

    public static native String commonrate();

    public static native String upmail();

    public static native String upd();

    public static native String getperm1();

    public static native String getprm1();

    public static native String getgrpReq();

    public static native String finalkey();

    public static native String sign1();

    public static native String sign2();

    public static native String image3();

    public static native String numbers();

    public static native String mainPhoto();

    public static native String mainPhototoken();

    public static native String main1();

    public static native String resource();

    /* -------------------------------------------------------------------------------
                                        TipActivity
       -------------------------------------------------------------------------------
    */

    public static native String gettips();

    public static native String getdone();

    public static native String getcas1();

    public static native String gettipsconstant();

    public static native String gettips1();

    public static native String gettips2();

    public static native String gettips3();

    /* -------------------------------------------------------------------------------
                                        TipsDetailActivity(Tips Detail)
       -------------------------------------------------------------------------------
    */

    public static native String getid();

    public static native String getcas2();

    public static native String getcontent1();

    /* -------------------------------------------------------------------------------
                                        MoreAppsActivity
       -------------------------------------------------------------------------------
    */

    public static native String moreurl();

    public static native String moreurl2();

    /* -------------------------------------------------------------------------------
                                        AdmireActivity
       -------------------------------------------------------------------------------
    */

    public static native String image4();

    /* -------------------------------------------------------------------------------
                                        Service
       -------------------------------------------------------------------------------
    */

    public static native String getValue1();

    public static native String getValue2();

    public static native String getValue3();

}