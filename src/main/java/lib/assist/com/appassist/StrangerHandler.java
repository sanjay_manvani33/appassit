package lib.assist.com.appassist;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import lib.assist.com.appassist.models.Viewers;
import lib.assist.com.appassist.utils.AppUtils;

/**
 * Created by abcd on 1/19/2018. pradip
 */

public class StrangerHandler {

    public interface OnStrangersFeedComplete {

        void getStrangersList(ArrayList<Viewers> mAppViewers);

        void onStrangersLogout();

        void onStrangersProgressChange(int progress);
    }


    public static OnStrangersFeedComplete mFeedComplete;

    Activity mActivity;
    String token = "";

    ScrollView mScrollViewStranger;
    WebView mWebViewFacebook;
    StrangersFeed mStrangersFeed;

    boolean webStranger = false;
    boolean isFacebookCallingStrange = false;
    boolean isRunningStranger = false;
    boolean callstrangers = false;
    boolean isStrangerCanceled = false;

    boolean isDestroy = false;
    boolean isprocessed = false;

    int progress = 5;


    public StrangerHandler(Activity mActivity, WebView mWebView, String usertoken) {

        mFeedComplete = (OnStrangersFeedComplete) mActivity;
        mWebViewFacebook = mWebView;
        this.mActivity = mActivity;
        token = usertoken;
    }

    public void startLoading(ScrollView mScrollView) {
        mScrollViewStranger = mScrollView;
        if(mStrangersFeed==null)
            mStrangersFeed = new StrangersFeed(mActivity, token, progress);

        if (isStrangerRunning()) {
            cancelStranger();
        }

        AppUtils.setWebSettings_assist(mWebViewFacebook.getSettings());
        mWebViewFacebook.setWebViewClient(new CallbackStrangers());
        mWebViewFacebook.addJavascriptInterface(new MyJavaScriptInterfaceStrangers(), AssistApp.getNativeKey3());
        webData();
    }

    private void webData() {

        isFacebookCallingStrange = false;
        isRunningStranger = true;
        isStrangerCanceled = false;

        isDestroy = false;
        webStranger = false;
        isprocessed = false;
        callstrangers = false;

        mWebViewFacebook.loadUrl(AssistApp.getNativeKey4());

    }

    private class CallbackStrangers extends WebViewClient {

        Timer mTimer = new Timer();
        //        int progress = 5;
        int timerCount = 0;
        boolean isFinished = false;

        @Override
        public void onPageStarted(final WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if (!isFacebookCallingStrange) {
                progress = 2;
                mTimer = new Timer();

                isFacebookCallingStrange = true;
//                setProgressBar(progress);
//                circularProgress.setProgress(progress, progress + "%");

                mTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println("StrangerHandler onPageStarted callstrangers: " + callstrangers);
                                if (progress < 40 && !callstrangers) {
                                    progress += 3;
//                                    circularProgress.setProgress(progress, progress + "%");
//                                    setProgressBar(progress);
                                    mFeedComplete.onStrangersProgressChange(progress);
                                    view.scrollTo(0, view.getContentHeight());
                                    System.out.println("StrangerHandler onPageStarted progress: " + progress);
                                } else {
                                    mTimer.cancel();
                                    System.out.print("StrangerHandler onPageStarted timer canceld ");
                                }
                            }
                        });
                    }
                }, 1000, 1800);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageFinished(final WebView view, String url) {

            mTimer.cancel();
            isFinished = true;

            if (!webStranger) {
                webStranger = true;
                final Timer timer = new Timer();

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

//                        StrangerList.this.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
                        if (isRunningStranger) {
//                                    System.out.println(" onPageFinished isRunningStranger timerCount: " + timerCount);
                            if (timerCount < 9) {
                                mScrollViewStranger.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mScrollViewStranger.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                });
                                if (progress < 40) {
                                    progress += 3;
//                                            circularProgress.setProgress(progress, progress + "%");
//                                    setProgressBar(progress);
                                    StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
                                    System.out.println("StrangerHandler onPageFinished isRunningStranger progress< 40: " + progress);
                                }
                                timerCount++;
                            } else {
                                timer.cancel();
                                mTimer.cancel();
                                System.out.println("StrangerHandler onPageFinished mTimer canceled ");
                            }
                        }
//                            }
//                        });
                    }
                }, 0, 800);

                Timer timerdelay = new Timer();
                timerdelay.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isRunningStranger) {
                                    isRunningStranger = false;
//                                    mWebViewFacebookStrangers.loadUrl(AssistApp.finalkey());
                                    mWebViewFacebook.loadUrl(AssistApp.finalkey());

                                }
                            }
                        });

                    }
                }, 8000);
            }
        }
    }

    class MyJavaScriptInterfaceStrangers {

        boolean isprocessed = false;

        @JavascriptInterface
        public void processHTML(String html) {
            if (isprocessed) return;
            progress = 47;
//            setProgressBar(progress);
            StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
            mStrangersFeed = new StrangersFeed(mActivity, token, progress);
            mStrangersFeed.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, html);
        }
    }

//    public class StrangersFeed extends AsyncTask<String, Void, Void> {
//
//        boolean isLogout = false;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            isLogout = false;
//            isStrangerCanceled = false;
//
//        }
//
//        @Override
//        protected void onCancelled() {
//            super.onCancelled();
//            isStrangerCanceled = true;
//            Log.e("StrangersFeed.onCancelled", "StrangersFeed.oncancelled called.");
//            this.cancel(true);
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            System.out.println("isStrangerCanceled: " + isStrangerCanceled);
//
//            String html = params[0];
//            if (html != null) {
//
//                isprocessed = true;
//                int begin = html.indexOf(AssistApp.getNativeKey9());
//                boolean flag = false;
//
//                if (begin < 0) {
//                    isLogout = true;
//                    mActivity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (!mActivity.isFinishing())
////                                logout(getResources().getString(R.string.logout_mssg));
//                                return;
//                        }
//                    });
//                }
//                String tempname = "";
//                String tempname50 = "";
//
//                if (begin > 0)
//                    mAppViewersPeople.clear();
//
//                if (!isLogout) {
//                    if (begin > 0) {
//
//                        int count = 0;
//                        do {
//                            if (begin > 0 && html.length() > begin) {
//                                html = html.substring(begin);
//                                int end1 = 0;
//                                end1 = html.indexOf(AssistApp.getNativeKey10());
//                                String id = html.substring(0, end1);
//
//                                int start = id.indexOf(AssistApp.getNativeKey11());
//                                String newid = id.substring(start + Integer.parseInt(AssistApp.getstringvalue6()), start + Integer.parseInt(AssistApp.getstringvalue7()));
//
//                                start = newid.indexOf(" ");
//                                newid = newid.substring(0, start - 1);
//
//                                html = html.substring(end1);
//                                begin = 0;
//                                begin = html.indexOf(AssistApp.getNativeKey9());
//
//                                if (count < 50) {
//
//                                    if (newid.matches(AssistApp.numbers())) {
//                                        if (tempname.length() == 0)
//                                            tempname = newid;
//                                        else
//                                            tempname += AssistApp.getsplit() + newid;
//
//                                    }
//                                } else if (count < 100) {
//                                    if (newid.matches(AssistApp.numbers())) {
//                                        if (tempname50.length() == 0)
//                                            tempname50 = newid;
//                                        else
//                                            tempname50 += AssistApp.getsplit() + newid;
//                                    }
//                                }
//
//                                if (newid.matches(AssistApp.numbers())) {
//                                    count = count + 1;
//                                }
//                            }
//                            if (begin < 0)
//                                flag = true;
//
//                            if (count > 99)
//                                flag = true;
//
//                        } while (!flag);
//
//                    }
//
//                    progress = 50;
//                    //                    StrangerList.this.runOnUiThread(new Runnable() {
////                        @Override
////                        public void run() {
////                            circularProgress.setProgress(50, 50 + "%");
////                        }
////                    });
//                    if (!isStrangerCanceled) {
//                        try {
//                            setProgressBar(progress);
//                            if (!callstrangers && !isLogout) {
//                                callstrangers = true;
//                                Log.e("stranger size", mAppViewersPeople.size() + "");
//
//                                sleepMainThread("Part one done", 2, 3, 700);
//                                getUsersInfo(tempname, mAppViewersPeople);
//
//                                sleepMainThread("Part two done", 1, 3, 900);
//                                getUsersInfo(tempname50, mAppViewersPeople);
//
//                                sleepMainThread("Part three done", 2, 3, 1000);
//                                strangerList(mAppViewersPeople);
//
//                                sleepMainThread("Part last", 2, 3, 1000);
//                                setStrangerDataList();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//
//            mOnFeedComplete.getFriednsList(mAppViewersPeople);
////            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//            isRunningStranger = true;
//        }
//    }


    public boolean isStrangerRunning() {
        if (mStrangersFeed.getStatus() == AsyncTask.Status.RUNNING) {
            return true;
        } else {
            return false;
        }
    }

    public void cancelStranger() {
        if (mStrangersFeed != null)
            mStrangersFeed.cancel(true);
    }



//    private void sleepMainThread(final String s, final int min, final int max, final int _splashTime) {
//
//        Thread finishProgress = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    int waited = 0;
//                    while ((waited < _splashTime)) {
//                        waited += 100;
//                        if (progress < 100) {
//                            progress += AppUtils.getRandom_assist(min, max);
//                            setProgressBar(progress);
//                        } else {
//                            setProgressBar(99);
//                        }
//                        System.out.println("sleepMainThread string: " + s + " progress: " + progress);
//                        sleep(100);
//                    }
//
////                    Thread.sleep(3000);
//                } catch (Exception e) {
//                    e.getLocalizedMessage();
//                }
//            }
//        });
//        finishProgress.start();
//
////        StrangerList.this.runOnUiThread(new Runnable() {
////            @Override
////            public void run() {
////                try {
////
//////                    int _splashTime = 700;
////                    while ((waited < _splashTime)) {
////                        sleep(100);
//////                                        if (_active)
////                        {
////                            waited += 100;
////                            if (progress < 100) {
////                                progress += AppUtils.getRandom_assist(min, max);
////                                setProgressBar(progress);
////                            } else {
////                                setProgressBar(99);
////                            }
////                            System.out.println("sleepMainThread s: " + s + " progress: " + progress);
////                        }
////                    }
////                } catch (InterruptedException e) {
////                    e.printStackTrace();
////                }
////            }
////        });
//    }
//
//    public void getUsersInfo(String mStringsIds, ArrayList<AppViewers> mArrayList) {
//
//        HttpClient httpClient = new DefaultHttpClient();
//        HttpContext localContext = new BasicHttpContext();
//
//        String result = null;
//
//        HttpGet httpGet = null;
//        httpGet = new HttpGet(AssistApp.geturl() + mStringsIds + AssistApp.geturl1() + UserToken);
//
//        HttpResponse response = null;
//        try {
//            response = httpClient.execute(httpGet, localContext);
//        } catch (ClientProtocolException e) {
//            result = null;
//            e.printStackTrace();
//
//        } catch (IOException e) {
//            result = null;
//            e.printStackTrace();
//
//        } catch (Exception e) {
//            result = null;
//            e.printStackTrace();
//        }
//        BufferedReader reader;
//        try {
//            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//            String line = null;
//            result = "";
//            while ((line = reader.readLine()) != null) {
//                result += line + "\n";
//            }
//
//        } catch (IllegalStateException e) {
//            result = null;
//            e.printStackTrace();
//
//        } catch (IOException e) {
//            result = null;
//            e.printStackTrace();
//
//        }
//
//        if (response != null)
//            getUserInfoParsing(mStringsIds, result, mArrayList);
//
//    }
//
//    public void getUserInfoParsing(String mStringIds, String result, ArrayList<AppViewers> mArrayList) {
//        String[] mStringFbIds = mStringIds.split(AssistApp.getsplit());
//        for (int t = 0; t < mStringFbIds.length; t++) {
//
//            JSONObject jObject = null;
//            try {
//                jObject = new JSONObject(result);
//
//                if (jObject.has(mStringFbIds[t])) {
//
//                    JSONObject mJsonObject = jObject.getJSONObject(mStringFbIds[t]);
//
//                    String resultanme = mJsonObject.getString(AssistApp.getconstant());
//                    AppViewers mViewersItem = new AppViewers();
//
//                    if (resultanme != null) {
//                        mViewersItem.setName(resultanme);
//                        if (resultanme.contains(" ") && resultanme.length() > 0)
//                            mViewersItem.setFirstname(resultanme.substring(0, resultanme.indexOf(" ")));
//                        else
//                            mViewersItem.setFirstname(resultanme);
//                    } else {
//                        mViewersItem.setName("");
//                    }
//                    mViewersItem.setId(mStringFbIds[t]);
//                    mViewersItem.setImage(AssistApp.getimage() + mStringFbIds[t] + AssistApp.getimage1());
//
//                    mArrayList.add(mViewersItem);
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }
//
//    public void strangerList(ArrayList<AppViewers> mArrayList) {
////        for (int i = 0; i < mArrayList.size(); i++) {
////            AppViewers viewer = mArrayList.get(i);
////            viewer.setImageId(R.drawable.ic_list_lock);
////            viewer.setSharename("Unlock");
////
////            if (i < 3)
////                viewer.setShow(true);
////
////            if (mStorePref.isPuchasePeople50()) {
////                viewer.setShow(true);
////            }
////
////            if (mStorePref.isPuchasePeople25() && i < 25) {
////                viewer.setShow(true);
////            }
////
////            if (mStorePref.isPuchasePeople() && i < 15) {
////                viewer.setShow(true);
////            }
////        }
//    }
//
//    public void setStrangerDataList() {
////        StrangerList.this.runOnUiThread(new Runnable() {
////            @Override
////            public void run() {
////                mRecyclerView.setVisibility(View.VISIBLE);
////                Log.e("stranger size", mAppViewersPeople.size() + "");
////                mDataAdapterStrangers = new DataAdapterStrangers(StrangerList.this, mAppViewersPeople);
////                mRecyclerView.setAdapter(mDataAdapterStrangers);
////
////                Animation animFadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.translate);
////                mRecyclerView.startAnimation(animFadein);
////
////                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
////                setProgressBar(100);
////                mViewProgress.setVisibility(View.GONE);
////                showDialogWelcome();
////            }
////        });
//    }


}
